import { useEffect, useRef, useState } from 'react';
/* criamos uma página que carrega os posts da API JSON PLACEHOLDER e fizemos toda a limpeza dos componentes para que não fosse gerado erros quando o usuário decidir sair do componente (navegar para outras páginas). Também vimos a útilização de um customHook que realiza o data fetching e não gera a renderização do componente em um looping já que estamos tratando os objetos. Isso estava acontecendo devido a uma chamada do useEffect que não tem a capacidade de comparar objetos e tanto o primeiro quanto o segundo parâmetro do nosso customHook são objetos, sendo assim foi necessário utilizar o useRef para criar uma referência e termos o ID atual para ser renderizado novamente sempre que o usuário clicar em alguma das divs. Isso despara uma ação que altera o valor do postID que por consequência (efeito) o useEffect é chamado novamente por ter o shouldLoad como dependência que só é chamado quando o componente precisa ser montado*/
/* eslint-disable no-unused-vars */

const isObjectEqual = (objA, objB) => {
  return JSON.stringify(objA) === JSON.stringify(objB);
};

export const useFetch = (url, options) => {
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(false);
  const [shouldLoad, setShouldLoad] = useState(false);
  const urlRef = useRef(url);
  const optionsRef = useRef(options);

  useEffect(() => {
    let changed = false;
    if (!isObjectEqual(url, urlRef.current)) {
      urlRef.current = url;
      changed = true;
    }

    if (!isObjectEqual(options, optionsRef.current)) {
      optionsRef.current = options;
      changed = true;
    }

    if (changed) {
      //foi criada uma função que retorna um valor para que o setShoudLoad não seja uma dependência desse useEffect
      setShouldLoad((s) => !s);
    }
  }, [url, options]);

  useEffect(() => {
    let wait = false;
    const controller = new AbortController();
    const signal = controller.signal;

    setLoading(true);
    const fetchData = async () => {
      await new Promise((r) => setTimeout(r, 3000));

      try {
        const response = await fetch(urlRef.current, { signal, ...optionsRef.current });
        const jsonResult = await response.json();

        if (!wait) {
          setResult(jsonResult);
          setLoading(false);
        }
      } catch (e) {
        if (!wait) {
          setLoading(false);
        }
        console.warn(e);
      }
    };

    fetchData();

    return () => {
      wait = true;
      controller.abort();
    };
  }, [shouldLoad]);

  return [result, loading];
};
