//customHook criado para realizar uma chamada async e tratar o estado dessa promise de modo que o componente Home irá mostrar um resultado na tela de acordo com esse status. Esse hook pode ser reutilizado para outras chamadas assincronas

/* eslint-disable no-unused-vars */
import { useCallback, useEffect, useState } from 'react';

const useAsync = (asyncFunction, shouldRun) => {
  const [state, setState] = useState({
    result: null,
    error: null,
    status: 'idle',
  });

  const run = useCallback(() => {
    setState({
      result: null,
      error: null,
      status: 'pending',
    });

    return asyncFunction()
      .then((response) => {
        setState({
          result: response,
          error: null,
          status: 'settled',
        });
      })
      .catch((error) => {
        setState({
          result: null,
          error: error,
          status: 'error',
        });
      });
  }, [asyncFunction]);

  useEffect(() => {
    if (shouldRun) {
      run();
    }
  }, [run, shouldRun]);

  return [run, state.result, state.error, state.status];
};

const fetchData = async () => {
  const data = await fetch('https://jsonplaceholder.typicode.com/posts');
  const json = await data.json();
  return json;
};

export const Home = () => {
  const [posts, setPosts] = useState(null);
  const [reFetchData, result, error, status] = useAsync(fetchData, true);

  function handleClick() {
    reFetchData();
  }

  switch (status) {
    case 'idle': {
      return <pre>Nada executando</pre>;
    }

    case 'pending': {
      return <pre>Loading...</pre>;
    }
    case 'error': {
      return <pre>{error.message}</pre>;
    }
    case 'settled': {
      return <pre onClick={handleClick}>{JSON.stringify(result, null, 2)}</pre>;
    }
  }

  return 'IXI';
};
