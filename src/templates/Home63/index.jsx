/* eslint-disable no-unused-vars */
import { useDebugValue, useEffect, useState } from 'react';

const useMediaQuery = (queryvalue, initialValue = false) => {
  const [match, setMatch] = useState(initialValue);

  useDebugValue(`Query: ${queryvalue}`);

  useEffect(() => {
    let isMounted = true;
    const matchMedia = window.matchMedia(queryvalue);

    const handleChange = () => {
      if (!isMounted) return;
      setMatch(!!matchMedia.matches);
    };

    matchMedia.addEventListener('change', handleChange);
    setMatch(!!matchMedia.matches);

    return () => {
      isMounted = false;
      matchMedia.removeEventListener('change', handleChange);
    };
  }, [queryvalue]);

  return match;
};

export const Home = () => {
  const huge = useMediaQuery('(min-width: 980px)');
  const big = useMediaQuery('(max-width: 979px) and (min-width: 768px)');
  const medium = useMediaQuery('(max-width: 767px) and (min-width: 321px)');
  const small = useMediaQuery('(max-width: 320px)');

  const background = huge ? 'green' : big ? 'red' : medium ? 'yellow' : small ? 'purple' : null;
  return <div style={{ fontsize: '60px', background }}>Oi</div>;
};
