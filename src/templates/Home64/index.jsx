/* eslint-disable no-unused-vars */

/*este exemplo foi realizado pelo professor Luiz Otavio
trata-se de um componente que possui um componente filho que é renderizado quando o botão de click muda o valor.Server para entendermos melhor como é feita a ordem de execução dos hooks, também vale a pena consultar o diagrama abaixo: https://github.com/donavon/hook-flow/blob/master/hook-flow.png
*/
import { useEffect, useLayoutEffect, useRef, useState } from 'react';

export const ReactHooks = () => {
  console.log('%cCHILD RENDER STARTING...', 'color: green');
  const renders = useRef(0);

  const [state1, setState1] = useState(() => {
    const state = new Date().toLocaleDateString();
    console.log('%cState Lazy initializer - (useState + InitialValue' + state, 'color: green');
    return state;
  });

  useEffect(() => {
    console.log('%cEmpty dependencies...', 'color: #dbc70f');

    return () => {
      console.log('%cuseEffect (Cleanup) -> Empty dependencies', 'color: #dbc70f');
    };
  }, []);

  useEffect(() => {
    console.log('%cNo dependencies', 'color: #dbc70f');
    renders.current += 1;
    return () => {
      console.log('%cuseEffect (Cleanup) -> No dependencies', 'color: #dbc70f');
    };
  });

  useLayoutEffect(() => {
    console.log('%cuseLayoutEffect', 'color: #e61a4d');

    return () => {
      console.log('%cuseLayoutEffect (Cleanup)', 'color: #e61a4d');
    };
  });

  useEffect(() => {
    console.log('%cuseEffect (UPDATE state1)' + state1, 'color: #dbc70f');
  }, [state1]);

  console.log('%cCHILD RENDER ' + renders.current + ' ENDING...', 'color: green');

  return <div onClick={() => setState1(new Date().toLocaleString('pt-br'))}>State: {state1}</div>;
};
export const Home = () => {
  const renders = useRef(0);

  useEffect(() => {
    renders.current += 1;
  });

  console.log(`%cPARENT RENDER ${renders.current} STARTING ...`, 'color: green');
  const [show, setShow] = useState(null);
  console.log('%cState initializer - (useState + InitialValue - ' + show, 'color: green');
  console.log(`%cPARENT RENDER ${renders.current} ENDING ...`, 'color: green');

  return (
    <div>
      <p onClick={() => setShow((s) => !s)}>Show Hooks</p>
      {show && <ReactHooks />}
    </div>
  );
};
