import { useContext } from 'react';
import { GlobalContext } from '../../context/AppContext';

// eslint-disable-next-line

export const P = () => {
  const theContext = useContext(GlobalContext);
  const {
    //eslint-disable-next-line
    contextState: { body, counter },
    setState,
  } = theContext;

  //desestruturação e uso do operetor spread para que o objeto atualize
  //assim o conteúdo no contexto não sofrerá mudança de estado além da prop
  return (
    <p
      onClick={() => {
        setState((s) => ({ ...s, counter: s.counter + 1 }));
      }}
    >
      {body}
    </p>
  );
};
