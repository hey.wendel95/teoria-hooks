import { loadPosts } from '../../context/PostsProvider/actions';
import { useContext, useEffect, useRef } from 'react';
import { PostsContext } from '../../context/PostsProvider/context';
import { CounterContext } from '../../context/CounterProvider/context';
import { incrementCounter, decrementCounter } from '../../context/CounterProvider/actions';
/*componente responsável por chamar o método dispatch do reducer
isso é feito através do useEffect que será chamado sempre que sua
dependência alterar seu valor*/
export const Posts = () => {
  const isMounted = useRef(true);

  const postsContext = useContext(PostsContext);
  const counterContext = useContext(CounterContext);

  // eslint-disable-next-line no-unused-vars
  const { postsState, postsDispatch } = postsContext;
  // eslint-disable-next-line no-unused-vars
  const { counterState, counterDispatch } = counterContext;
  /*chama a action loadPosts (async) que faz o data fetching
e em seguida dispara uma ação para o reducer com o resultado do fetching
contido numa prop chamada de payload onde esto
contidos os posts */

  console.log(isMounted.current);
  useEffect(() => {
    loadPosts(postsDispatch).then((dispatch) => {
      if (isMounted.current) {
        dispatch();
      }
    });

    return () => {
      isMounted.current = false;
    };
  }, [postsDispatch]);

  return (
    <div>
      <button onClick={() => incrementCounter(counterDispatch)}>Counter: {counterState.counter}+</button>

      <button onClick={() => decrementCounter(counterDispatch)}>Counter: {counterState.counter}-</button>
      <h1>Componente Posts</h1>
      {postsState.loading && (
        <p>
          <strong>Carregando posts...</strong>
        </p>
      )}

      {postsState.posts.map((p) => (
        <p key={p.id}>{p.title}</p>
      ))}
    </div>
  );
};
