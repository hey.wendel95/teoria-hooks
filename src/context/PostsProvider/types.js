//types das actions, ficando tudo em um documento é mais fácil de padronizar
//interessante manter o padrão CAPS_LOCK para essas types
//utilizaremos os types para definir qual action será disparada
//ex: Posts carregando, posts carregados, erro ao carregar posts, etc.

export const POSTS_LOADING = 'POSTS_LOADING';
export const POSTS_SUCESS = 'POSTS_SUCESS';
