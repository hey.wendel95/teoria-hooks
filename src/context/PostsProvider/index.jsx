import P from 'prop-types';
import { useReducer } from 'react';
import { PostsContext } from './context';
import { reducer } from './reducer';
import { data } from './data';

/*componente responsável por prover as props de seus filhos
para que não haja o retrabalho de acessar componente por componente
até chegar na prop desejada. Isso é feito através do .Provider
que é herdado do createContext.
Esse provider recebe um value que pode ser um objeto que contem
um estado e uma função. Nesse caso usamos o reducer*/

export const PostsProvider = ({ children }) => {
  const [postsState, postsDispatch] = useReducer(reducer, data);

  return <PostsContext.Provider value={{ postsState, postsDispatch }}>{children}</PostsContext.Provider>;
};

PostsProvider.propTypes = {
  children: P.node.isRequired,
};
