//essa é uma organização específica do professor Luiz
//o intuito é separar em diretórios para que se obtenha um padrão de projeto
// dessa forma é mais simples de encontrar erros e reutilizar códigos também

//actions são as ações que deverão acontecer declaradas como funções
import * as types from './types';

//action para carregar os posts e disparar de acordo com o type
export const loadPosts = async (dispatch) => {
  dispatch({ type: types.POSTS_LOADING });

  const postsRaw = await fetch('https://jsonplaceholder.typicode.com/posts');
  const posts = await postsRaw.json();
  console.log('Carreguei os posts');

  return () => dispatch({ type: types.POSTS_SUCESS, payload: posts });
};
