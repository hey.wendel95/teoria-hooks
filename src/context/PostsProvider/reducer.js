import * as types from './types';

/*o reducer irá manipular o estado de acordo com a action que ele receber
por isso os dois parâmetros state, action.
é importante lembrar que o estado deve sempre ser mantido
*/
export const reducer = (state, action) => {
  //switch case para que a função só aconteça para cada case
  //ou se não houver nenhum dos cases
  switch (action.type) {
    /*usando o return como break e trazendo o objeto que contém
    o estado anterior, os posts vindo da action em payload e
    uma flag com para tratar de carregamento dos posts*/
    case types.POSTS_SUCESS: {
      console.log(action.type);
      return { ...state, posts: action.payload, loading: false };
    }
    case types.POSTS_LOADING: {
      console.log(action.type);
      return { ...state, loading: true };
    }
  }

  console.log('Não econtrei a action', action.type);
  return { ...state };
};
