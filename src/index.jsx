import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './templates/App';
import { Home } from './templates/Home66';

ReactDOM.render(
  <React.StrictMode>
    <Home />
  </React.StrictMode>,
  document.getElementById('root'),
);
